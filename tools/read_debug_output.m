clc;
clear all;
close all;

bb_input_dir = '../beagleboard/';
py_input_dir = '../pc/';

plot_adc = 1;
plot_pf = 1;
plot_fft = 1;
plot_enet = 1;
plot_py = 1;

pulse_to_plot = 1;

num_plots = plot_py + plot_enet + plot_fft + plot_pf + plot_adc;
current_plot_num = 1;

if (num_plots > 0)
   figure;
end

%%%%%%%%%%
%% ADC
%%%%%%%%%%
    if(plot_adc == 1)
        filename = fullfile(bb_input_dir, 'recv.dat');
        fid = fopen(filename, 'r');
        if (fid < 0)
           fprintf('Error opening ADC input "%s"\n',filename)
        else
           recv_raw = fread(fid, inf, 'int16','ieee-le');
           fclose(fid);

           fid = fopen(fullfile(bb_input_dir, 'sync.dat'), 'r');
           sync_raw = fread(fid, inf, 'int16','ieee-le');
           fclose(fid);

           h = ones(1,16) / 16;
           mv_avg = filter(h, 1, sync_raw);

		     subplot(num_plots,1,current_plot_num);
		     current_plot_num = current_plot_num + 1;
           hold on;
           plot(sync_raw);
           plot(recv_raw,'r');
           plot(mv_avg, '-k');
           hold off;
           axis tight;
           ylabel('Amplitude [tick]');
           xlabel('Sample');
           title('ADC: Rx and Sync')
        end
    end

%%%%%%%%%%
%% PF
%%%%%%%%%%
    if(plot_pf == 1)
        filename = sprintf(fullfile(bb_input_dir,'PF_1_%d.dat'),pulse_to_plot);
        fid = fopen(filename, 'r');
        if (fid < 0)
           fprintf('Error opening Pulse Framer input "%s"\n',filename)
        else
           pf_raw = fread(fid, inf, 'float32','ieee-le');
           fclose(fid);

           subplot(num_plots,1,current_plot_num);
      	  current_plot_num = current_plot_num + 1;
           hold on;
           plot(real(pf_raw), 'b');
           grid on;
           axis tight;
           ylabel('Amplitude [tick]');
           xlabel('Sample');
           title('PF: Real')
        end
    end

%%%%%%%%%%
%% FFT
%%%%%%%%%%
    if(plot_fft == 1)
        filename = sprintf(fullfile(bb_input_dir,'FFT%d.dat'),pulse_to_plot);
        fid = fopen(filename, 'r');
        if (fid < 0)
           fprintf('Error opening FFT input "%s"\n', filename)
        else
           fft_raw = fread(fid, inf, 'float32','ieee-le');
           fft_complex = fft_raw(1:2:end) + 1i*fft_raw(2:2:end);
           fft_db = 20*log10(abs(fft_complex));
           fclose(fid);

           subplot(num_plots,1,current_plot_num);
           current_plot_num = current_plot_num + 1;
           hold on;
           plot(real(fft_complex), 'b');
           plot(imag(fft_complex), 'r');
           grid on;
           axis tight;
           ylabel('Amplitude [tick]');
           xlabel('Sample');
           title('FFT: Real and Complex')
        end
    end    
    
%%%%%%%%%%
%% ENET
%%%%%%%%%%
    if(plot_enet == 1)
        filename = sprintf(fullfile(bb_input_dir,'Eth%d.dat'),pulse_to_plot);
        fid = fopen(filename, 'r');
        if (fid < 0)
           fprintf('Error opening ENET input "%s"\n', filename)
        else
           eth_raw = fread(fid, inf, 'float32','ieee-le');
           eth_complex = eth_raw(1:2:end) + 1i*eth_raw(2:2:end);
           eth_db = 20*log10(abs(eth_complex));
           fclose(fid);

           subplot(num_plots,1,current_plot_num);
           current_plot_num = current_plot_num + 1;
           hold on;
           plot(real(eth_complex), 'b');
           plot(imag(eth_complex), 'r');
           grid on;
           axis tight;
           ylabel('Amplitude [tick]');
           xlabel('Sample');
           title('Enet: Real and Complex')
        end
    end
    
%%%%%%%%%%
%% Python
%%%%%%%%%%   
    if(plot_py == 1)
        filename = sprintf(fullfile(py_input_dir,'python_%d.bin'),pulse_to_plot);
        fid = fopen(filename, 'r');
        if (fid < 0)
           fprintf('Error opening Python input "%s"\n', filename)
        else
           py_raw = fread(fid, inf, 'float64','ieee-le');
           py_complex = py_raw(1:2:end) + 1i*py_raw(2:2:end);
           py_db = 20*log10(abs(py_complex));
           fclose(fid);

           subplot(num_plots,1,current_plot_num);
           current_plot_num = current_plot_num + 1;
           hold on;
           plot(real(py_complex), 'b');
           plot(imag(py_complex), 'r');
           grid on;
           axis tight;
           ylabel('Amplitude [tick]');
           xlabel('Sample');
           title('Python: Real and Complex')
        end
    end

fclose('all');
