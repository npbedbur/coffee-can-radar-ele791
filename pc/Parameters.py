class Parameters:   
    #Common radar params
    num_rc = 200;
    
    #Debug scripts
    write_pulses_to_file = 1;
    
    #Processing params
    processing_type = "doppler" # "doppler", "2 pulse cancler"
    
    #Doppler radar params
    num_pulses_per_dwell = 8;
    
    #MTI radar params
    
    #Plot params        
    pulses_to_plot = (num_pulses_per_dwell-1)*10; #If in MTI mode
    fixed_clim = False
    clim = [0, 10]
    
    #Enet params
    #pc_ip = '127.0.0.1'
    #pc_ip = '192.168.1.50'
    pc_ip = '10.6.6.234'
    radar_to_pc_port = 7910    
    rx_buffer_size = num_rc*num_pulses_per_dwell*4*2;
