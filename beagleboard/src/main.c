#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <fftw3.h>

#include "common.h"

#include "adc_thread.h"
#include "pulse_framer_thread.h"
#include "fft_thread.h"
#include "eth_thread.h"

// These are the data buffers shared between each thread
signed short sync_data0[SAMPLES_TO_REQUEST];
signed short rcv_data0[SAMPLES_TO_REQUEST];
signed short sync_data1[SAMPLES_TO_REQUEST];
signed short rcv_data1[SAMPLES_TO_REQUEST];
static int adc_data_ready = 0;
static int adc_data_buf_num = 0;
static int adc_num_samples = 0;
static int pulse_framer_ready = 0;
static int fft_data_ready = 0;

int main()
{
   pthread_t thread0, thread1, thread2, thread3;
   int  iret0, iret1, iret2, iret3;
	int i;

	float ** pulse_buffer;//[MAX_PULSES_PER_DWELL][MAX_SAMPLES_PER_PULSE];
	fftwf_complex ** fft_result;
//[MAX_PULSES_PER_DWELL][MAX_FREQ_BINS_PER_PULSE];

	pulse_buffer = (float **)malloc(sizeof(float *)*MAX_PULSES_PER_DWELL);
	for (i = 0; i < MAX_PULSES_PER_DWELL; i++)
	{
		pulse_buffer[i] = (float *)malloc(sizeof(float)*MAX_SAMPLES_PER_PULSE);		
	}

	fft_result = (fftwf_complex **)malloc(sizeof(fftw_complex *)*MAX_PULSES_PER_DWELL);
	for (i = 0; i < MAX_PULSES_PER_DWELL; i++)
	{
		fft_result[i] = (fftwf_complex *)fftwf_alloc_complex(C_NUM_FFT_BINS);
	}

	pthread_mutex_t adc_out_mutex = PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t adc_out_cond = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t pulse_framer_out_mutex = PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t pulse_framer_out_cond = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t fft_out_mutex = PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t fft_out_cond = PTHREAD_COND_INITIALIZER;

	// Setup the parameters for each thread (mutex, ptrs to data buffers)
	AdcThreadInfo_s adc_thread_params;
	PulseFramerThreadInfo_s pulse_thread_params;
	FftThreadInfo_s fft_thread_params;
	EthThreadInfo_s eth_thread_params;
	 
	adc_thread_params.sync_ch_buffer0 = sync_data0;
	adc_thread_params.recv_ch_buffer0 = rcv_data0;
	adc_thread_params.sync_ch_buffer1 = sync_data1;
	adc_thread_params.recv_ch_buffer1 = rcv_data1;
	adc_thread_params.output_data_mutex = &adc_out_mutex;
	adc_thread_params.output_data_cond = &adc_out_cond;
	adc_thread_params.output_data_ready_flag = &adc_data_ready;
	adc_thread_params.output_data_buf_num = &adc_data_buf_num;
	adc_thread_params.output_num_samples = &adc_num_samples;

	pulse_thread_params.sync_ch_buffer0 = sync_data0;
	pulse_thread_params.recv_ch_buffer0 = rcv_data0;
	pulse_thread_params.sync_ch_buffer1 = sync_data1;
	pulse_thread_params.recv_ch_buffer1 = rcv_data1;
	pulse_thread_params.recv_pulses = (float **)pulse_buffer;
	pulse_thread_params.input_data_mutex = &adc_out_mutex;
	pulse_thread_params.input_data_cond = &adc_out_cond;
	pulse_thread_params.input_data_ready_flag = &adc_data_ready;
	pulse_thread_params.input_data_buf_num = &adc_data_buf_num;
	pulse_thread_params.output_data_mutex = &pulse_framer_out_mutex;
	pulse_thread_params.output_data_cond = &pulse_framer_out_cond;
	pulse_thread_params.input_num_samples = &adc_num_samples;
	pulse_thread_params.output_data_ready_flag = &pulse_framer_ready;

	fft_thread_params.pulse_buffer = (float **)pulse_buffer;
	fft_thread_params.fft_result = (fftwf_complex **)fft_result;
	fft_thread_params.input_data_mutex = &pulse_framer_out_mutex;
	fft_thread_params.input_data_cond = &pulse_framer_out_cond;
	fft_thread_params.output_data_mutex = &fft_out_mutex;
	fft_thread_params.output_data_cond = &fft_out_cond;
	fft_thread_params.input_data_ready_flag = &pulse_framer_ready;
   fft_thread_params.output_data_ready_flag = &fft_data_ready;

	eth_thread_params.fft_result = (fftwf_complex **)fft_result;
	eth_thread_params.input_data_mutex = &fft_out_mutex;
	eth_thread_params.input_data_cond = &fft_out_cond;
   eth_thread_params.input_data_ready_flag = &fft_data_ready;

   /* Create independent threads each of which will execute function */
	iret0 = pthread_create( &thread0, NULL, adc_thread, (void*) &adc_thread_params);
   iret1 = pthread_create( &thread1, NULL, pulse_framer_thread, (void*) &pulse_thread_params);
   iret2 = pthread_create( &thread2, NULL, fft_thread, (void*) (void*) &fft_thread_params);
   iret3 = pthread_create( &thread3, NULL, eth_thread, (void*) (void*) &eth_thread_params);
	 
   /* Wait till threads are complete before main continues. Unless we  */
   /* wait we run the risk of executing an exit which will terminate   */
   /* the process and all threads before the threads have completed.   */
	pthread_join( thread0, NULL);
   pthread_join( thread1, NULL);
   pthread_join( thread2, NULL);
   pthread_join( thread3, NULL);
	
	if (iret0 != 0 || iret1 != 0 || iret2 != 0 || iret3 != 0)
	{ 
		return -1;
	}
 	else
	{
		return 0;
	}
}
	 


