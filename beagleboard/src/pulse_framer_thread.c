#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

#include "pulse_framer_thread.h"
#include "common.h"

void *pulse_framer_thread( void *ptr )
{
	PulseFramerThreadInfo_s * info = (PulseFramerThreadInfo_s*) ptr;

	int i;
   int j;
	int num_samples = 0;
	signed short *sync_channel = NULL;
	signed short *recv_channel = NULL;
	float ** output_buffer = info->recv_pulses;
	int current_pulse_num = 0;
	int current_sample_num = 0; // per pulse
	int copy = 0;
   int total_dwell_num = 0;
   float current_pulse_mean = 0;

   /*
	float mv_avg_val = 0;
	float last_mv_avg_input = 0;
	float last_mv_avg_val = 0;
   */

#ifdef _FILEIO_
   int written = 0;
   char filename[256];
#endif

   while(1)
	{
		pthread_mutex_lock(info->input_data_mutex);

		// wait on data from the ADC
      while (*(info->input_data_ready_flag)==0)
		{
			pthread_cond_wait(info->input_data_cond, info->input_data_mutex);
		}

      total_dwell_num++;

      //printf("PF\n");		

      copy = 0;
		current_pulse_num = 0;
		current_sample_num = 0;
		num_samples = *(info->input_num_samples);

      //printf("PulseFramer, buf %d, %d samples\n", *info->input_data_buf_num, num_samples);

		// get pointers to correct buffer
		if (*(info->input_data_buf_num) == 0)
		{
			// buffer 0
			sync_channel = info->sync_ch_buffer0;
			recv_channel = info->recv_ch_buffer0;
		}
		else
		{
			// buffer 1
			sync_channel = info->sync_ch_buffer1;
			recv_channel = info->recv_ch_buffer1;
		}

		// get mutex for output data
      pthread_mutex_lock(info->output_data_mutex);
		if (*(info->output_data_ready_flag) == 0)
		{			

			// preload filter
         /*			
         for (i = 0; i < C_MV_AVG_LEN && i < num_samples; i++)
			{
				mv_avg_val += sync_channel[i];
				last_mv_avg_input = sync_channel[i];
			}
			mv_avg_val = (float)mv_avg_val / C_MV_AVG_LEN;
         */

			// frame data
			//for (i = C_MV_AVG_LEN; i < num_samples; i++)
         for (i = 1; i < num_samples; i++)
			{	
		
				// Check for exceeding number of pulses in a dwell
            if (current_pulse_num > MAX_PULSES_PER_DWELL - 1)
				{
               // Wait for next dwell
					break;
				}			

				// Update moving average filter
				/*
            last_mv_avg_val = mv_avg_val;
				mv_avg_val += sync_channel[i] / C_MV_AVG_LEN;
				mv_avg_val -= last_mv_avg_input / C_MV_AVG_LEN;
				last_mv_avg_input = sync_channel[i];
            */

            // Check for rising edge of sync
				//if (mv_avg_val > 0 && last_mv_avg_val < 0 && copy == 0)
            if (sync_channel[i] > 30000 && sync_channel[i-1] < 30000 && copy == 0)
				{
					
					copy = 1;
					current_sample_num = 0;

               // Skip some samples at beginning of each pulse
					i = i + NUM_SKIPPED_SAMPLES;

				}
				
            // Check for reaching end of copy
				if (current_sample_num >= C_MAX_GOOD_RANGE_CELLS && copy == 1)
				{
      
               // Fill in zeros for remaining samples
               for (j = current_sample_num; j < MAX_SAMPLES_PER_PULSE; j++)
               {
                  output_buffer[current_pulse_num][j] = 0;
               }

               // Calculate mean (DC)
               current_pulse_mean = 0;
               for (j = 0; j < C_MAX_GOOD_RANGE_CELLS; j++)
               {
                  current_pulse_mean += output_buffer[current_pulse_num][j];
               }
               current_pulse_mean = current_pulse_mean / C_MAX_GOOD_RANGE_CELLS;

               // DC subtract
               for (j = 0; j < C_MAX_GOOD_RANGE_CELLS; j++)
               {
                  output_buffer[current_pulse_num][j] -= current_pulse_mean;
               }

					copy = 0;
					current_pulse_num++;
               current_sample_num = 0;
				}			
				
            // Copy logic
            if (copy == 1)
            {
				   // Copy samples from recv buffer to output for FFT
				   output_buffer[current_pulse_num][current_sample_num] = (float)recv_channel[i];
				   current_sample_num++;
            }
			}

#ifdef _FILEIO_
			// write to file for debugging
			if(written < 2)
			{
				 written++;
             for (i = 0; i < MAX_PULSES_PER_DWELL; i++)
             {
                sprintf(filename, "PF_%d_%d.dat", total_dwell_num, i);
			       writeToFile(filename,(u_char *)output_buffer[i], MAX_SAMPLES_PER_PULSE*sizeof(float));
             }
			}
#endif

			// unlock mutex
			*(info->input_data_ready_flag) = 0; // reset flag variable
			pthread_mutex_unlock(info->input_data_mutex);

			// signal that framed data is ready for FFT
			*(info->output_data_ready_flag) = 1;
      	pthread_cond_signal(info->output_data_cond);
		}
		else
		{
			printf("! Error, FFT did not reset flag for pulse framer\n");
		}
      pthread_mutex_unlock(info->output_data_mutex);


   }
}

