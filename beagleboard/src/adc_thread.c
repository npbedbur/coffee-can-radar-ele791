#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <fftw3.h>

#include "adc_thread.h"
#include "common.h"

/* global data */

static snd_pcm_sframes_t (*readi_func)(snd_pcm_t *handle, void *buffer, snd_pcm_uframes_t size);
static snd_pcm_sframes_t (*readn_func)(snd_pcm_t *handle, void **bufs, snd_pcm_uframes_t size);
static snd_pcm_t *handle;

static struct {
	snd_pcm_format_t format;
	unsigned int channels;
	unsigned int rate;
} hwparams, rhwparams;

static int quiet_mode = 0;
static int open_mode = 0;
static snd_pcm_stream_t stream = SND_PCM_STREAM_CAPTURE;
static int mmap_flag = 0; // not sure what mmap does differently
static int nonblock = 0;
//u_char *audiobuf1 = NULL;
static int current_buffer = 0;
static snd_pcm_uframes_t chunk_size = 0;

#ifndef _DEBUG_
static size_t bits_per_sample;
static int interleaved = 0;
static unsigned period_time = 0;
static unsigned buffer_time = 0;
static snd_pcm_uframes_t period_frames = SAMPLES_TO_REQUEST;
static snd_pcm_uframes_t buffer_frames = SAMPLES_TO_REQUEST;
static int avail_min = -1;
static int start_delay = 1;
static int stop_delay = 0;
#endif

static int monotonic = 0;
static int can_pause = 0;
static int verbose = 0;
static size_t bits_per_frame;
static size_t chunk_bytes;

/*******************************************************************************
 * Thread entry point
 ******************************************************************************/
void *adc_thread( void *ptr )
{
	// Cast ptr to thread info struct
	AdcThreadInfo_s * adc_info = (AdcThreadInfo_s*) ptr;

	char *pcm_name = "default";
	int err;
	int do_device_list = 0, do_pcm_list = 0;
	snd_pcm_info_t *info;

	snd_pcm_info_alloca(&info);

	// Hard coded parameters for sound input
	rhwparams.format = SND_PCM_FORMAT_S16_LE; // 16 bit little endian
	rhwparams.rate = 44100; // 44100 Hz
	rhwparams.channels = 2; // stereo

	if (do_device_list) {
		if (do_pcm_list) pcm_list();
		device_list();
		goto __end;
	} else if (do_pcm_list) {
		pcm_list();
		goto __end;
	}

	err = snd_pcm_open(&handle, pcm_name, stream, open_mode);
	if (err < 0) {
		printf("Sound error\n");
		exit(-1);
	}

	if ((err = snd_pcm_info(handle, info)) < 0) {
		printf("info error\n");
		exit(-1);
	}

	if (nonblock) {
		err = snd_pcm_nonblock(handle, 1);
		if (err < 0) {
			printf("nonblock setting error\n");
			exit(-1);
		}
	}

	// This is an initial allocation, we reallocate later after
	// determining what the HW can actually support
	chunk_size = 1024;
	hwparams = rhwparams;

	if (mmap_flag) {
		readi_func = snd_pcm_mmap_readi;
		readn_func = snd_pcm_mmap_readn;
	} else {
		readi_func = snd_pcm_readi; // interleaved
		readn_func = snd_pcm_readn; // non-interleaved
	}

	printf("Running capture once with null\n");
	capture(adc_info);

	if (verbose==2)
		putchar('\n');
	snd_pcm_close(handle);
	handle = NULL;
	/*free(audiobuf1);*/
      __end:
	snd_config_update_free_global();
	prg_exit(EXIT_SUCCESS);
	/* avoid warning */
	exit(0);

}

/*******************************************************************************
 *  device_list - prints devices available for sound
 ******************************************************************************/
void device_list(void)
{
	snd_ctl_t *handle;
	int card, err, dev, idx;
	snd_ctl_card_info_t *info;
	snd_pcm_info_t *pcminfo;
	snd_ctl_card_info_alloca(&info);
	snd_pcm_info_alloca(&pcminfo);

	card = -1;
	if (snd_card_next(&card) < 0 || card < 0) {
		printf("no soundcards found...\n");
		return;
	}
	printf(_("**** List of %s Hardware Devices ****\n"),
	       snd_pcm_stream_name(stream));
	while (card >= 0) {
		char name[32];
		sprintf(name, "hw:%d", card);
		if ((err = snd_ctl_open(&handle, name, 0)) < 0) {
			printf("control open...(%i)\n", card);
			goto next_card;
		}
		if ((err = snd_ctl_card_info(handle, info)) < 0) {
			printf("control hardware info (%i)\n", card);
			snd_ctl_close(handle);
			goto next_card;
		}
		dev = -1;
		while (1) {
			unsigned int count;
			if (snd_ctl_pcm_next_device(handle, &dev)<0)
				printf("snd_ctl_pcm_next_device\n");
			if (dev < 0)
				break;
			snd_pcm_info_set_device(pcminfo, dev);
			snd_pcm_info_set_subdevice(pcminfo, 0);
			snd_pcm_info_set_stream(pcminfo, stream);
			if ((err = snd_ctl_pcm_info(handle, pcminfo)) < 0) {
				if (err != -ENOENT)
					printf("control digital audio info (%i)\n", card);
				continue;
			}
			printf(_("card %i: %s [%s], device %i: %s [%s]\n"),
				card, snd_ctl_card_info_get_id(info), snd_ctl_card_info_get_name(info),
				dev,
				snd_pcm_info_get_id(pcminfo),
				snd_pcm_info_get_name(pcminfo));
			count = snd_pcm_info_get_subdevices_count(pcminfo);
			printf( _("  Subdevices: %i/%i\n"),
				snd_pcm_info_get_subdevices_avail(pcminfo), count);
			for (idx = 0; idx < (int)count; idx++) {
				snd_pcm_info_set_subdevice(pcminfo, idx);
				if ((err = snd_ctl_pcm_info(handle, pcminfo)) < 0) {
					printf("control digital audio playback info (%i)", card);
				} else {
					printf(_("  Subdevice #%i: %s\n"),
						idx, snd_pcm_info_get_subdevice_name(pcminfo));
				}
			}
		}
		snd_ctl_close(handle);
	next_card:
		if (snd_card_next(&card) < 0) {
			printf("snd_card_next\n");
			break;
		}
	}
}

/*******************************************************************************
 * List PCM devices available
 ******************************************************************************/
void pcm_list(void)
{
	void **hints, **n;
	char *name, *descr, *descr1, *io;
	const char *filter;

	if (snd_device_name_hint(-1, "pcm", &hints) < 0)
		return;
	n = hints;
	filter = stream == SND_PCM_STREAM_CAPTURE ? "Input" : "Output";
	while (*n != NULL) {
		name = snd_device_name_get_hint(*n, "NAME");
		descr = snd_device_name_get_hint(*n, "DESC");
		io = snd_device_name_get_hint(*n, "IOID");
		if (io != NULL && strcmp(io, filter) != 0)
			goto __end;
		printf("%s\n", name);
		if ((descr1 = descr) != NULL) {
			printf("    ");
			while (*descr1) {
				if (*descr1 == '\n')
					printf("\n    ");
				else
					putchar(*descr1);
				descr1++;
			}
			putchar('\n');
		}
	      __end:
	      	if (name != NULL)
	      		free(name);
		if (descr != NULL)
			free(descr);
		if (io != NULL)
			free(io);
		n++;
	}
	snd_device_name_free_hint(hints);
}

/*******************************************************************************
 *	Subroutine to clean up before exit.
 ******************************************************************************/
void prg_exit(int code) 
{
	if (handle)
		snd_pcm_close(handle);
	exit(code);
}

/*******************************************************************************
 * Queries sound card for available formats (# bits, endianness, etc)
 ******************************************************************************/
void show_available_sample_formats(snd_pcm_hw_params_t* params)
{
	snd_pcm_format_t format;

	fprintf(stderr, "Available formats:\n");
	for (format = 0; format < SND_PCM_FORMAT_LAST; format++) {
		if (snd_pcm_hw_params_test_format(handle, params, format) == 0)
			fprintf(stderr, "- %s\n", snd_pcm_format_name(format));
	}
}

/*******************************************************************************
 * Sets PCM parameters for recording
 ******************************************************************************/
void set_params(void)
{

#ifndef _DEBUG_
	snd_pcm_hw_params_t *params;
	snd_pcm_sw_params_t *swparams;
	snd_pcm_uframes_t buffer_size;
	int err;
	size_t n;
	unsigned int rate;

	snd_pcm_uframes_t start_threshold, stop_threshold;
	snd_pcm_hw_params_alloca(&params);
	snd_pcm_sw_params_alloca(&swparams);
	err = snd_pcm_hw_params_any(handle, params);
	if (err < 0) {
		printf("Broken configuration for this PCM: no configurations available\n");
		prg_exit(EXIT_FAILURE);
	}
	if (mmap_flag) {
		snd_pcm_access_mask_t *mask = alloca(snd_pcm_access_mask_sizeof());
		snd_pcm_access_mask_none(mask);
		snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_INTERLEAVED);
		snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_NONINTERLEAVED);
		snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_COMPLEX);
		err = snd_pcm_hw_params_set_access_mask(handle, params, mask);
	} else if (interleaved)
		err = snd_pcm_hw_params_set_access(handle, params,
						   SND_PCM_ACCESS_RW_INTERLEAVED);
	else
		err = snd_pcm_hw_params_set_access(handle, params,
						   SND_PCM_ACCESS_RW_NONINTERLEAVED);
	if (err < 0) {
		printf("Access type not available\n");
		prg_exit(EXIT_FAILURE);
	}
	err = snd_pcm_hw_params_set_format(handle, params, hwparams.format);
	if (err < 0) {
		printf("Sample format non available\n");
		show_available_sample_formats(params);
		prg_exit(EXIT_FAILURE);
	}
	err = snd_pcm_hw_params_set_channels(handle, params, hwparams.channels);
	if (err < 0) {
		printf("Channels count non available\n");
		prg_exit(EXIT_FAILURE);
	}

#if 0
	err = snd_pcm_hw_params_set_periods_min(handle, params, 2);
	assert(err >= 0);
#endif
	rate = hwparams.rate;
	err = snd_pcm_hw_params_set_rate_near(handle, params, &hwparams.rate, 0);
	assert(err >= 0);
	if ((float)rate * 1.05 < hwparams.rate || (float)rate * 0.95 > hwparams.rate) {
		if (!quiet_mode) {
			char plugex[64];
			const char *pcmname = snd_pcm_name(handle);
			fprintf(stderr, _("Warning: rate is not accurate (requested = %iHz, got = %iHz)\n"), rate, hwparams.rate);
			if (! pcmname || strchr(snd_pcm_name(handle), ':'))
				*plugex = 0;
			else
				snprintf(plugex, sizeof(plugex), "(-Dplug:%s)",
					 snd_pcm_name(handle));
			fprintf(stderr, _("         please, try the plug plugin %s\n"),
				plugex);
		}
	}
	rate = hwparams.rate;
	if (buffer_time == 0 && buffer_frames == 0) {
		err = snd_pcm_hw_params_get_buffer_time_max(params,
							    &buffer_time, 0);
		assert(err >= 0);
		if (buffer_time > 500000)
			buffer_time = 500000;
	}
	if (period_time == 0 && period_frames == 0) {
		if (buffer_time > 0)
			period_time = buffer_time / 4;
		else
			period_frames = buffer_frames / 4;
	}
	if (period_time > 0)
		err = snd_pcm_hw_params_set_period_time_near(handle, params,
							     &period_time, 0);
	else
		err = snd_pcm_hw_params_set_period_size_near(handle, params,
							     &period_frames, 0);
	assert(err >= 0);
	if (buffer_time > 0) {
		err = snd_pcm_hw_params_set_buffer_time_near(handle, params,
							     &buffer_time, 0);
	} else {
		err = snd_pcm_hw_params_set_buffer_size_near(handle, params,
							     &buffer_frames);
	}
	assert(err >= 0);
	monotonic = snd_pcm_hw_params_is_monotonic(params);
	can_pause = snd_pcm_hw_params_can_pause(params);
	err = snd_pcm_hw_params(handle, params);
	if (err < 0) {
		printf("Unable to install hw params:\n");
		prg_exit(EXIT_FAILURE);
	}
	snd_pcm_hw_params_get_period_size(params, &chunk_size, 0);
	snd_pcm_hw_params_get_buffer_size(params, &buffer_size);
	if (chunk_size == buffer_size) {
		printf("Can't use period equal to buffer size (%lu == %lu)\n",
		      chunk_size, buffer_size);
		prg_exit(EXIT_FAILURE);
	}
	snd_pcm_sw_params_current(handle, swparams);
	if (avail_min < 0)
		n = chunk_size;
	else
		n = (double) rate * avail_min / 1000000;
	err = snd_pcm_sw_params_set_avail_min(handle, swparams, n);

	/* round up to closest transfer boundary */
	n = buffer_size;
	if (start_delay <= 0) {
		start_threshold = n + (double) rate * start_delay / 1000000;
	} else
		start_threshold = (double) rate * start_delay / 1000000;
	if (start_threshold < 1)
		start_threshold = 1;
	if (start_threshold > n)
		start_threshold = n;
	err = snd_pcm_sw_params_set_start_threshold(handle, swparams, start_threshold);
	assert(err >= 0);
	if (stop_delay <= 0) 
		stop_threshold = buffer_size + (double) rate * stop_delay / 1000000;
	else
		stop_threshold = (double) rate * stop_delay / 1000000;
	err = snd_pcm_sw_params_set_stop_threshold(handle, swparams, stop_threshold);
	assert(err >= 0);

	if (snd_pcm_sw_params(handle, swparams) < 0) {
		printf("unable to install sw params:\n");
		prg_exit(EXIT_FAILURE);
	}

	bits_per_sample = snd_pcm_format_physical_width(hwparams.format);
	bits_per_frame = bits_per_sample * hwparams.channels;
	chunk_bytes = chunk_size * bits_per_frame / 8;
	
	printf("Capturing %d bytes in each chunk\n", (int)chunk_bytes);

	// fprintf(stderr, "real chunk_size = %i, frags = %i, total = %i\n", chunk_size, setup.buf.block.frags, setup.buf.block.frags * chunk_size);

	/* show mmap buffer arragment */
	if (mmap_flag && verbose) {
		const snd_pcm_channel_area_t *areas;
		snd_pcm_uframes_t offset, size = chunk_size;
		int i;
		err = snd_pcm_mmap_begin(handle, &areas, &offset, &size);
		if (err < 0) {
			printf("snd_pcm_mmap_begin problem\n");
			prg_exit(EXIT_FAILURE);
		}
		for (i = 0; i < hwparams.channels; i++)
			fprintf(stderr, "mmap_area[%i] = %p,%u,%u (%u)\n", i, areas[i].addr, areas[i].first, areas[i].step, snd_pcm_format_physical_width(hwparams.format));
		/* not required, but for sure */
		snd_pcm_mmap_commit(handle, offset, 0);
	}

	buffer_frames = buffer_size;	/* for position test */

#else

	bits_per_frame = 32;

#endif

}

/*******************************************************************************
 * Pauses recording (unused)
 ******************************************************************************/
void do_pause(void)
{
	int err;
	unsigned char b;

	if (!can_pause) {
		fprintf(stderr, _("\rPAUSE command ignored (no hw support)\n"));
		return;
	}
	err = snd_pcm_pause(handle, 1);
	if (err < 0) {
		printf("pause push error\n");
		return;
	}
	while (1) {
		while (read(fileno(stdin), &b, 1) != 1);
		if (b == ' ' || b == '\r') {
			while (read(fileno(stdin), &b, 1) == 1);
			err = snd_pcm_pause(handle, 0);
			if (err < 0)
				printf("pause release error\n");
			return;
		}
	}
}

/*******************************************************************************
 * I/O error handler 
 ******************************************************************************/
void xrun(void)
{
	snd_pcm_status_t *status;
	int res;
	
	snd_pcm_status_alloca(&status);
	if ((res = snd_pcm_status(handle, status))<0) {
		printf("status error\n");
		prg_exit(EXIT_FAILURE);
	}
	if (snd_pcm_status_get_state(status) == SND_PCM_STATE_XRUN) {
		if (monotonic) {
#ifdef HAVE_CLOCK_GETTIME
			struct timespec now, diff, tstamp;
			clock_gettime(CLOCK_MONOTONIC, &now);
			snd_pcm_status_get_trigger_htstamp(status, &tstamp);
			timermsub(&now, &tstamp, &diff);
			fprintf(stderr, _("%s!!! (at least %.3f ms long)\n"),
				 _("overrun"),
				diff.tv_sec * 1000 + diff.tv_nsec / 10000000.0);
#else
			fprintf(stderr, "%s !!!\n", _("underrun"));
#endif
		} else {
			struct timeval now, diff, tstamp;
			gettimeofday(&now, 0);
			snd_pcm_status_get_trigger_tstamp(status, &tstamp);
			timersub(&now, &tstamp, &diff);
			fprintf(stderr, _("%s!!! (at least %.3f ms long)\n"),
				_("overrun"),
				diff.tv_sec * 1000 + diff.tv_usec / 1000.0);
		}

		if ((res = snd_pcm_prepare(handle))<0) {
			printf("xrun: prepare error: %s", snd_strerror(res));
			prg_exit(EXIT_FAILURE);
		}
		return;		/* ok, data should be accepted again */
	} if (snd_pcm_status_get_state(status) == SND_PCM_STATE_DRAINING) {

		if (stream == SND_PCM_STREAM_CAPTURE) {
			fprintf(stderr, _("capture stream format change? attempting recover...\n"));
			if ((res = snd_pcm_prepare(handle))<0) {
				printf("xrun(DRAINING): prepare error: %s", snd_strerror(res));
				prg_exit(EXIT_FAILURE);
			}
			return;
		}
	}
	printf("read/write error\n");
	prg_exit(EXIT_FAILURE);
}

/*******************************************************************************
 * I/O suspend handler (unused)
 ******************************************************************************/
void suspend(void)
{
	int res;

	if (!quiet_mode)
		fprintf(stderr, _("Suspended. Trying resume. ")); fflush(stderr);
	while ((res = snd_pcm_resume(handle)) == -EAGAIN)
		sleep(1);	/* wait until suspend flag is released */
	if (res < 0) {
		if (!quiet_mode)
			fprintf(stderr, _("Failed. Restarting stream. ")); fflush(stderr);
		if ((res = snd_pcm_prepare(handle)) < 0) {
			printf("suspend: prepare error\n");
			prg_exit(EXIT_FAILURE);
		}
	}
	if (!quiet_mode)
		fprintf(stderr, _("Done.\n"));
}

/*
 *  read function
 */

/*******************************************************************************
 * Reads a chunk of interleaved samples
 ******************************************************************************/
ssize_t pcm_read(u_char *data, size_t rcount)
{
	ssize_t r; // number of samples received in current iteration of below loop
	size_t result = 0; // counter for current number of bytes received
	size_t count = rcount; // decrementing counter to determine when to exit fcn

	if (count != chunk_size) {
		count = chunk_size;
	}

	while (count > 0) {
		r = readi_func(handle, data, count);
		if (r == -EAGAIN || (r >= 0 && (size_t)r < count)) {
			snd_pcm_wait(handle, 100);
		} else if (r == -EPIPE) {
			xrun();
		} else if (r == -ESTRPIPE) {
			suspend();
		} else if (r < 0) {
			printf("read error: %s", snd_strerror(r));
			prg_exit(EXIT_FAILURE);
		}
		if (r > 0) {
			result += r;
			count -= r;
			data += r * bits_per_frame / 8;
		}
	}
	return rcount;
}

/*******************************************************************************
 * Reads a chunk of non-interleaved samples
 ******************************************************************************/
ssize_t pcm_readv(u_char *sync_ch, u_char *recv_ch, size_t rcount)
{
	ssize_t r;
	size_t result = 0;
	size_t count = chunk_size;

	void *bufs[2];
	bufs[1] = sync_ch;
	bufs[0] = recv_ch;

	while (count > 0) {


		r = readn_func(handle, bufs, count);
		if (r == -EAGAIN || (r >= 0 && (size_t)r < count)) {
			snd_pcm_wait(handle, 100);
		} else if (r == -EPIPE) {
			xrun();
		} else if (r == -ESTRPIPE) {
			suspend();
		} else if (r < 0) {
			printf("readv error: %s\n", snd_strerror(r));
			prg_exit(EXIT_FAILURE);
		}
		if (r > 0) {
			result += r;
			count -= r;
		}
	}
	return result;
}

/*******************************************************************************
 * setting the globals for playing raw data 
 ******************************************************************************/
void init_raw_data(void)
{
	hwparams = rhwparams;
}

/*******************************************************************************
 * Prints a fancy message to the console
 ******************************************************************************/
void header()
{
	if (!quiet_mode) {
		fprintf(stderr, "Recording\n");
		fprintf(stderr, "%s, ", snd_pcm_format_description(hwparams.format));
		fprintf(stderr, _("Rate %d Hz, "), hwparams.rate);
		if (hwparams.channels == 1)
			fprintf(stderr, _("Mono"));
		else if (hwparams.channels == 2)
			fprintf(stderr, _("Stereo"));
		else
			fprintf(stderr, _("Channels %i"), hwparams.channels);
		fprintf(stderr, "\n");
	}
}

/*******************************************************************************
 * Writes a chunk of bytes to a file (used for debug)
 ******************************************************************************/
void writeToFile(char * fileName, u_char * buf, size_t numBytes)
{
	//unsigned short * samples = (unsigned short *)buf;
	//size_t i;
	//size_t numSamples = numBytes / 4;
	FILE * outfp = fopen(fileName,"w+");
	printf("Writing file, filename = %s\n", fileName);
	if (outfp)
	{
		fwrite(buf, sizeof(u_char), numBytes, outfp);
		//for (i = 0; i < 32 ; i+=2)
		//{
		//	printf("%ud %ud\n", samples[i], samples[i+1]);
		//}
		fclose(outfp); 
	}
}
/******************************************************************************
 *  Main capture loop
 ******************************************************************************/
void capture(AdcThreadInfo_s * info)
{
	size_t c;
	size_t f;

#ifdef _DEBUG_
	//int i;
#endif

#ifdef _FILEIO_
	int written = 0;
#endif

#ifdef _FILEI_
   FILE * sync_fin = NULL;
   FILE * recv_fin = NULL;
   size_t result;

   sync_fin = fopen("syncin.dat","r");
   recv_fin = fopen("recvin.dat","r");
   
   if (sync_fin == NULL || recv_fin == NULL) {printf ("! Input file error\n"); exit (1);}

   // obtain file size:
   rewind (sync_fin);
   rewind (recv_fin);

#endif

	//struct timespec tv;
	//double time_in_mill;

	/* display verbose output to console */
	header();

	/* setup sound hardware */
	set_params();

	c = chunk_bytes;
	f = c * 8 / bits_per_frame;

	/* capture loop */
	while (1) {
		// get time of sample (debugging)
		//clock_gettime(CLOCK_MONOTONIC, &tv);
		//time_in_mill = tv.tv_sec * 1000 + tv.tv_nsec / 1000000;
		//printf("%d frames, %010.5lf ms\n", f, time_in_mill);

#ifdef _FILEI_
	   sleep(1);

      c = SAMPLES_TO_REQUEST * 2 * sizeof (signed short);
      f = c * 8/ bits_per_frame;

      // copy the files into the buffers:
      //printf("Reading %d bytes\n", c/2);
      result = fread (info->recv_ch_buffer0,1,c/2,sync_fin);
      if (result != c/2) {printf ("Reading error\n"); exit (3);}
      rewind (sync_fin);

      result = fread (info->sync_ch_buffer0,1,c/2,recv_fin);
      if (result != c/2) {printf ("Reading error\n"); exit (3);}
      rewind (recv_fin);

	  /* for (i = 0; i < f; i++)
	   {
         
         info->recv_ch_buffer0[i] = (32768) * sin((double)2*3.1415*i/50);

		   if (i % 1800 > 900)
		   {
			   info->sync_ch_buffer0[i] = 50;
		   }
		   else if (i % 1800 < 895)
		   {
			   info->sync_ch_buffer0[i] = -50;
		   }
		   else
		   {
			   // Create some small number between [-7, 7]
			   info->sync_ch_buffer0[i] = -7 + (rand() % 15);
		   }

	   }*/

       *(info->output_num_samples) = f;


#else

		// ping pong between two input buffers so we can write while
		// other threads are using filled in buffer
		if (current_buffer == 0)
		{
			if (pcm_readv((u_char *)info->sync_ch_buffer0, (u_char *)info->recv_ch_buffer0, f) != f)
			{
				break;
			}	
			else
			{
				*(info->output_num_samples) = f;
			}
		}
		else
		{
			if (pcm_readv((u_char *)info->sync_ch_buffer1, (u_char *)info->recv_ch_buffer1, f) != f)
			{
				break;
			}
			else
			{
				*(info->output_num_samples) = f;
			}
		}

#endif

		// lock mutex 
      	pthread_mutex_lock(info->output_data_mutex);

		// check if flag was reset by pulse framer
		if (*info->output_data_ready_flag == 0)
		{

#ifdef _FILEIO_
          // write data to file
		    if (!written)
		    {
			    written = 1;
			    writeToFile("sync.dat", (u_char *)info->sync_ch_buffer0, c/2);
			    writeToFile("recv.dat", (u_char *)info->recv_ch_buffer0, c/2);
		    }	
#endif

			// signal that ADC data is ready
			*info->output_data_ready_flag = 1;
			*info->output_data_buf_num = current_buffer;
	    	pthread_cond_signal(info->output_data_cond);
		}
		else
		{
			printf("! ADC thread could not access buffer, did pulse framer finish?\n");
		}	

		// unlock mutex
   	    pthread_mutex_unlock(info->output_data_mutex);

#ifndef _DEBUG_
		// increment buffer
		current_buffer = (current_buffer + 1) % 2;
#endif

	}

	printf("! Exiting on buffer read error\n");

}





