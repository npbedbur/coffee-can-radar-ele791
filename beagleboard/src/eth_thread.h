#ifndef ETH_THREAD_H
#define ETH_THREAD_H

#include <pthread.h>

#define C_PORT 7910
//#define C_SERVER_ADDR "192.168.1.50"
#define C_SERVER_ADDR "10.6.6.234"
#define C_RANGE_SAMPLES_TO_SEND 200

typedef struct {
	fftwf_complex ** fft_result;
	pthread_mutex_t *input_data_mutex;
	pthread_cond_t *input_data_cond;
   int * input_data_ready_flag;      
} EthThreadInfo_s;

void *eth_thread( void *ptr );

#endif

