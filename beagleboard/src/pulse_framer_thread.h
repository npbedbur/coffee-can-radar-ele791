#ifndef PULSE_FRAMER_THREAD_H
#define PULSE_FRAMER_THREAD_H

#include <fftw3.h>

#define NUM_SKIPPED_SAMPLES 32

#define C_MV_AVG_LEN 16
#define C_MAX_GOOD_RANGE_CELLS 512

typedef struct {
	signed short * sync_ch_buffer0;
	signed short * recv_ch_buffer0; 
	signed short * sync_ch_buffer1;
	signed short * recv_ch_buffer1;
	float ** recv_pulses;
	pthread_mutex_t *input_data_mutex;
	pthread_cond_t *input_data_cond;
	int * input_data_ready_flag;
	int * input_data_buf_num;
	int * input_num_samples;
	pthread_mutex_t *output_data_mutex;
	pthread_cond_t *output_data_cond;
	int * output_data_ready_flag;
} PulseFramerThreadInfo_s;

void *pulse_framer_thread( void *ptr );

#endif

