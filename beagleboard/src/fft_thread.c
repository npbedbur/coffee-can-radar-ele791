#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <fftw3.h>

#include "fft_thread.h"
#include "common.h"

void *fft_thread( void *ptr)
{
#ifdef _FILEIO_
   int written = 0;
   char filename[256];
#endif

	float *in;
	fftwf_complex *out;
	fftwf_plan plan_forward;
	int j;
	
	FftThreadInfo_s * info = (FftThreadInfo_s *)ptr;

	while(1)
	{
      pthread_mutex_lock(info->input_data_mutex);

		// wait on pulse data
		while(*(info->input_data_ready_flag) == 0)
		{
      	pthread_cond_wait( info->input_data_cond, info->input_data_mutex);
		}

     	
		
      // signal eth block
		pthread_mutex_lock(info->output_data_mutex);
      //printf("FFT lock!\n");
      if (*(info->output_data_ready_flag) == 0)
      {
      
         //cycle through each pulse/row
         for ( j = 0; j < MAX_PULSES_PER_DWELL; j++)
         {
            in = (float *)info->pulse_buffer[j];
            out = (fftwf_complex *)info->fft_result[j]; 

            plan_forward = fftwf_plan_dft_r2c_1d ( C_NUM_FFT_BINS, in, out, FFTW_ESTIMATE );

            // perform FFT
            fftwf_execute ( plan_forward );
            //printf("FFT exe!\n");
            fftwf_destroy_plan(plan_forward);

   #ifdef _FILEIO_
            // write to file for debugging
            if(written < MAX_PULSES_PER_DWELL)
            {
                written++;

                sprintf(filename, "FFT%d.dat", j);
                writeToFile(filename,(u_char *)out, C_NUM_FFT_BINS*sizeof(fftwf_complex));
            }
   #endif
            
         }
         //printf("FFT unlock!\n");
         *(info->input_data_ready_flag) = 0;
         pthread_mutex_unlock(info->input_data_mutex);

         *(info->output_data_ready_flag) = 1;
      }
		pthread_cond_signal(info->output_data_cond);
		pthread_mutex_unlock(info->output_data_mutex);


   }
}
